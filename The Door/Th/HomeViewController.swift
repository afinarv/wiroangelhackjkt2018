//
//  HomeViewController.swift
//  Fear Mini Challenge
//
//  Created by Afina R. Vinci on 18/07/18.
//  Copyright © 2018 afinarv. All rights reserved.
//

import UIKit
import AVFoundation
import AudioToolbox

class HomeViewController: UIViewController {
    
    @IBOutlet weak var homeImgView: UIImageView!
    @IBOutlet weak var coverView: UIView!
    @IBOutlet weak var corridorImgView: UIImageView!
    @IBOutlet weak var corridorContainerView: UIView!
    var audioPlayer0 = AVAudioPlayer()
    var audioPlayer1 = AVAudioPlayer()
    var audioPlayer2 = AVAudioPlayer()
    var hasOpened = false
    
    
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        playAudio()
        let startGesture = UITapGestureRecognizer(target: self, action: #selector(zooming))
        corridorContainerView.addGestureRecognizer(startGesture)
        //let wallTapGesture = UITapGestureRecognizer(target: self, action: #selector(showScreenshotEffect))
        //homeImgView.addGestureRecognizer(wallTapGesture)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        coverView.alpha = 1
        coverView.isHidden = false
        UIView.animate(withDuration: 2, delay: 0, options: .curveLinear, animations: {
            self.coverView.alpha = 0
        })
    }
    
    //MARK: - zooming corridor
    @objc func zooming() {
        coverView.alpha = 1
        UIView.animate(withDuration: 5, delay: 0, options: .curveLinear, animations: {
            self.corridorContainerView.transform = CGAffineTransform(scaleX: 3, y: 3)
        }) { (true) in
            UIView.animate(withDuration: 2, animations: {
                self.coverView.alpha = 0
            }, completion: nil)
        }
        
        UIView.animate(withDuration: 2, delay: 3, options: .curveLinear, animations: {
            self.corridorContainerView.alpha = 0
        }, completion: nil)
        
        if hasOpened {
            showScreenshotEffect()
        }
    }
    
    //MARK: - blink effect
    
    func showScreenshotEffect() {
        
        let snapshotView = UIView()
        snapshotView.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(snapshotView)
        view.bringSubview(toFront: snapshotView)
        
        let constraints:[NSLayoutConstraint] = [
            snapshotView.topAnchor.constraint(equalTo: view.topAnchor),
            snapshotView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            snapshotView.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            snapshotView.bottomAnchor.constraint(equalTo: view.bottomAnchor)
        ]
        NSLayoutConstraint.activate(constraints)
        
        snapshotView.backgroundColor = UIColor.black
        snapshotView.alpha = 0.0
        
        UIView.animate(withDuration: 2.2, animations: {
            snapshotView.alpha = 0.8
        }) { _ in
            UIView.animate(withDuration: 2.2, animations: {
                snapshotView.alpha = 0.0
            }, completion: { (true) in
                self.showScreenshotEffect()
            })
            //snapshotView.removeFromSuperview()
        }
    }
    
    //MARK: - audio playing
    func playAudio() {
        let path = Bundle.main.path(forResource:"darkshadow", ofType:"mp3")
        let url = URL(fileURLWithPath: path!)
        do {
            audioPlayer0 = try AVAudioPlayer(contentsOf: url)
            audioPlayer0.play()
            audioPlayer0.numberOfLoops = -1
        } catch {
            print("cant play audio")
        }
    }
    
    func playAudio1() {
        let path = Bundle.main.path(forResource:"inmynightmare", ofType:"m4a")
        let url = URL(fileURLWithPath: path!)
        do {
            audioPlayer1 = try AVAudioPlayer(contentsOf: url)
            audioPlayer1.play()
            audioPlayer1.numberOfLoops = -1
        } catch {
            print("cant play audio")
        }
    }
    
    func playAudio2() {
        let path = Bundle.main.path(forResource:"horror", ofType:"m4a")
        let url = URL(fileURLWithPath: path!)
        do {
            audioPlayer2 = try AVAudioPlayer(contentsOf: url)
            audioPlayer2.play()
            audioPlayer2.numberOfLoops = -1
        } catch {
            print("cant play audio")
        }
    }
    
    
    @IBAction func enterFirstSecondRoom(_ sender: UIButton) {
        let feedbackGenerator = UINotificationFeedbackGenerator()
        feedbackGenerator.notificationOccurred(.success)
        
        if hasOpened {
            if sender.tag == 0 {
                performSegue(withIdentifier: "toFirstRoom", sender: self)
            } else {
                performSegue(withIdentifier: "toSecondRoom", sender: self)
            }
        } else {
            sender.tag = 0
            hasOpened = true
            performSegue(withIdentifier: "toFirstRoom", sender: self)
        }
    }
    
    @IBAction func enterFinalRoom(_ sender: UIButton) {
        let notification = UINotificationFeedbackGenerator()
        notification.notificationOccurred(.error)
        
        performSegue(withIdentifier: "toLastRoom", sender: self)
        //let impact = UIImpactFeedbackGenerator(style: .medium)
        //impact.impactOccurred()
    }
    
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "toFirstRoom" {
            audioPlayer0.setVolume(0.8, fadeDuration: 1.5)
            playAudio1()
        } else if segue.identifier == "toSecondRoom" {
            audioPlayer1.setVolume(0.8, fadeDuration: 1.5)
            playAudio2()
        } else { //final room
            //masukin audio apa
            
        }
    }
    
    //MARK: - prepare for unwind
    @IBAction func prepareForUnwind(segue: UIStoryboardSegue) {
    }
    
    
    
    //this is heavy long vibration
    //AudioServicesPlayAlertSound(SystemSoundID(kSystemSoundID_Vibrate))
}

//: Playground - noun: a place where people can play

import UIKit

var str = "Hello, playground"

class Person {
    var name: String
    var age: Int
    init(_ name: String, _ age: Int) {
        self.name = name
        self.age = age
    }
    
    func birthday() {
        self.age += 1
        print("Happy birthday, \(self.name)")
    }
}

class Youtuber: Person {
    var channelName: String
    var subscribers: Int
    init(_ name: String, _ age: Int, channelName: String, subscribers: Int) {
        self.channelName = channelName
        self.subscribers = subscribers
        super.init(name, age)
    }
}

let handy = Person("Handy", 22)

let hayantu = Youtuber("Hayantu", 76, channelName: "HayantuChannel", subscribers: 89282980)
hayantu.name = "hayantu ganti nama"
hayantu.age = 78
hayantu.birthday()

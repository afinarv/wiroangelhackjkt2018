//
//  SecondViewController.swift
//  NanoChallenge 10-7
//
//  Created by Afina R. Vinci on 10/07/18.
//  Copyright © 2018 afinarv. All rights reserved.
//

import UIKit

class SecondViewController: UIViewController {

    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var mainLabel: UILabel!
    var sign = ""
    var emotions = ["a car accident", "choked", "drowned in the sea", "be left alone"]
    @IBOutlet weak var secondLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        mainLabel.text = sign
        for a in signs {
            if a == sign {
                mainLabel.text = a
                imgView.image = UIImage(named: a)
                let randomIndex = Int(arc4random_uniform(UInt32(emotions.count)))
                secondLabel.text = "You will \(emotions[randomIndex]) today"
                break
            }
        }
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

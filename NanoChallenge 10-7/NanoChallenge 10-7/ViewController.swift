//
//  ViewController.swift
//  NanoChallenge 10-7
//
//  Created by Afina R. Vinci on 10/07/18.
//  Copyright © 2018 afinarv. All rights reserved.
//

import UIKit

var signs = ["Capricorn", "Aquarius", "Pisces", "Taurus", "Aries", "Cancer", "Gemini", "Virgo", "Leo", "Libra", "Scorpio", "Sagitarius"]

class ViewController: UIViewController, UITextFieldDelegate {

    @IBOutlet weak var textField: UITextField!
    @IBOutlet weak var button: UIButton!
    var sign = ""
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        button.addTarget(self, action: #selector(buttonClick(_:)), for: .touchUpInside)
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    
    @IBAction func textFieldChanged(_ sender: UITextField) {
        sign = textField.text!
    }
    
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
//        sign = textField.text!
        var isValid = false
        for a in signs {
            if sign == a {
                isValid = true
                break
            }
        }
        
        if isValid {
            validSign()
        } else {
            invalidSign()
        }
        
        return true
    }
    
    @objc func buttonClick(_ sender: UIButton) {
        var isValid = false
        for a in signs {
            if sign == a {
                isValid = true
                break
            }
        }
        
        if isValid {
            validSign()
        } else {
            invalidSign()
        }
    }
    
    func validSign() {
        let alertView = UIAlertController(title: sign, message: "Are you sure you want to engage in such an unscientific matter like astrological prophecy?", preferredStyle: .alert)
        let okAction = UIAlertAction(title: "Yes", style: .default) { (okAction) in
            self.performSegue(withIdentifier: "toSecond", sender: self)
        }
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel) { (cancelAction) in
            self.dismiss(animated: true, completion: nil)
        }
        alertView.addAction(okAction)
        alertView.addAction(cancelAction)
        self.present(alertView, animated: true, completion: nil)
    }
    
    func invalidSign() {
        let invalidAlert = UIAlertController(title: "Invalid Sign", message: "You are inputing an invalid sign name", preferredStyle: .alert)
        let okAction = UIAlertAction(title: "OK", style: .default) { (okAction) in
            self.dismiss(animated: true, completion: nil)
        }
        invalidAlert.addAction(okAction)
        self.present(invalidAlert, animated: true, completion: nil)
    }
    
    
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let destination = segue.destination as! SecondViewController
        destination.sign = sign
        
    }


}






















/*
 enum Sign: String {
 "Capricorn",
  "Aquarius",
  "Pisces",
  "Taurus",
  "Aries",
  "Cancer",
  "Gemini",
  "Virgo",
  "Leo",
  "Libra",
  "Scorpio",
  "Sagitarius"
 }*/

//
//  ViewController.swift
//  BMI Calculator
//
//  Created by Afina R. Vinci on 09/07/18.
//  Copyright © 2018 afinarv. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var bmiScoreLabel: UILabel!
    @IBOutlet weak var bmiDescriptionLabel: UILabel!
    @IBOutlet weak var wTitleLabel: UILabel!
    @IBOutlet weak var weightLabel: UILabel!
    @IBOutlet weak var hTitleLabel: UILabel!
    @IBOutlet weak var heightLabel: UILabel!
    var weight = Float()
    var height = Float()
    var bmi = Float()
    
    @IBOutlet weak var resultTitleLabel: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }

    @IBAction func weightSliderChanged(_ sender: UISlider) {
        weight = sender.value
        self.weightLabel.text = String(format: "%.1f", weight) // "3.14"("\(weight)")
        calculateBMI(weight, height)
    }
    
    
    @IBAction func heightSliderChanged(_ sender: UISlider) {
        height = sender.value / 100
        self.heightLabel.text = String(format: "%.1f", sender.value) // "3.14"("\(sender.value)")
        calculateBMI(weight, height)
    }
    
    func calculateBMI(_ weight: Float, _ height: Float) {
        bmi = weight / (height*height)
    
        bmiScoreLabel.text = String(format: "%.1f", bmi) // "3.14""\(bmi)"
        bmiDescriptionLabel.text = (bmiDescription(ofScore: bmi)).0
        bmiScoreLabel.textColor = (bmiDescription(ofScore: bmi)).1
        bmiDescriptionLabel.textColor = (bmiDescription(ofScore: bmi)).1
    }
    
    func bmiDescription(ofScore: Float) -> (String, UIColor) {
    
        if ofScore == 0.0 {
            return ("-", UIColor.red)
        } else if ofScore <= 15.0 {
            return ("Very Severely Underweight", UIColor.red)
        } else if ofScore <= 16.0 {
            return ("Severely Underweight", UIColor.red)
        } else if ofScore <= 18.5 {
            return ("Underweight", UIColor.orange)
        } else if ofScore <= 25 {
            return ("Normal", UIColor.green)
        } else if ofScore <= 30 {
            return ("Overweight", UIColor.orange)
        } else if ofScore <= 35 {
            return ("Moderately Obese", UIColor.orange)
        } else if ofScore <= 40 {
            return ("Severely Obese", UIColor.red)
        } else {
            return ("Very Severely Obese", UIColor.red)
        }
    }
    
    @IBAction func changeBgColor(_ sender: UISegmentedControl) {
        if sender.selectedSegmentIndex == 0 {
            self.mainView.backgroundColor = UIColor.white
            titleLabel.textColor = UIColor.black
            wTitleLabel.textColor = UIColor.black
            hTitleLabel.textColor = UIColor.black
            resultTitleLabel.textColor = UIColor.black
        } else {
            self.mainView.backgroundColor = UIColor.black
            titleLabel.textColor = UIColor.white
            wTitleLabel.textColor = UIColor.white
            hTitleLabel.textColor = UIColor.white
            resultTitleLabel.textColor = UIColor.white
        }
    }
    

}


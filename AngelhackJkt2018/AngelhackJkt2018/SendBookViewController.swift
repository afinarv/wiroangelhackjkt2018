//
//  SendBookViewController.swift
//  AngelhackJkt2018
//
//  Created by Afina R. Vinci on 04/08/18.
//  Copyright © 2018 afinarv. All rights reserved.
//

import UIKit
import Firebase

class SendBookViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    var ref: DatabaseReference!
    var data = [[String:Any]]()
    var index = Int()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupDatabase()
        
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func setupDatabase() {
        ref = Database.database().reference().child("librarydata")
        ref.observe(.value, with: { (snapshot) in
            if snapshot.childrenCount > 0 {
                print(snapshot.childrenCount)
                
                for eachQuote in snapshot.children.allObjects as! [DataSnapshot] {
                    let quoteObject = eachQuote.value as? [String: AnyObject]
                    self.data.append(quoteObject!)
                }
                self.tableView.reloadData()
                
//                self.data = (snapshot.value as? [[String:Any]])!
//                print(self.data)
                
            }
            
        }) { (error) in
            print(error.localizedDescription)
            
            
        }
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "toLibraryDetail" {
            let destination = segue.destination as! LibraryDetailViewController
            destination.libraryData = self.data[index]
        }
    }
 
    @IBAction func backAction(_ sender: UIBarButtonItem) {
        self.navigationController?.popViewController(animated: true)
    }
}

extension SendBookViewController: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return data.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell") as! SendBookCell
        cell.nameLabel.text = data[indexPath.row]["name"] as! String
        
        let city = data[indexPath.row]["city_name"] as! String
        let province = data[indexPath.row]["province_name"] as! String
        let location = "\(city), \(province)"
        cell.locationLabel.text = location
        
        cell.categoryLabel.text = data[indexPath.row]["book_categories"] as! String
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print("select row \(indexPath.row)")
        self.index = indexPath.row
        performSegue(withIdentifier: "toLibraryDetail", sender: self)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 105
    }
}

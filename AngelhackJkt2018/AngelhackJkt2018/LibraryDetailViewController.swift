//
//  LibraryDetailViewController.swift
//  AngelhackJkt2018
//
//  Created by Afina R. Vinci on 04/08/18.
//  Copyright © 2018 afinarv. All rights reserved.
//

import UIKit
import MapKit

class LibraryDetailViewController: UIViewController {

    let locationManager = CLLocationManager()
    @IBOutlet weak var mapView: MKMapView!
    var libraryData = [String:Any]()
    
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var categoryLabel: UILabel!
    @IBOutlet weak var locationLabel: UILabel!
    @IBOutlet weak var avgvisitorLabel: UILabel!
    @IBOutlet weak var contactLabel: UILabel!
    

    override func viewDidLoad() {
        super.viewDidLoad()
        nameLabel.text = libraryData["name"] as! String
        categoryLabel.text = libraryData["book_categories"] as! String

        let city = libraryData["city_name"] as! String
        let province = libraryData["province_name"] as! String
        let location = "\(city), \(province)"
        locationLabel.text = location
        
        avgvisitorLabel.text = "Average visitor per day: \(libraryData["avg_visitor"]) people"
        let contact = "Contact: \(libraryData["contactname"] as! String) \(libraryData["contactnumber"] as! String)"
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    


}
extension LibraryDetailViewController: CLLocationManagerDelegate {
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        if status == .authorizedWhenInUse {
            locationManager.requestLocation()
        }
    }
    
    func locationManager(_ manager:CLLocationManager, didUpdateLocations locations: [CLLocation] ){
        if let location = locations.first {
            let span = MKCoordinateSpanMake(0.05, 0.05)
            let region = MKCoordinateRegion(center: location.coordinate, span: span)
            mapView.setRegion(region, animated: true)
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print("error::\(error)")
    }
}

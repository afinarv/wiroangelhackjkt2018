//
//  SendBookCell.swift
//  AngelhackJkt2018
//
//  Created by Afina R. Vinci on 04/08/18.
//  Copyright © 2018 afinarv. All rights reserved.
//

import UIKit

class SendBookCell: UITableViewCell {

    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var categoryLabel: UILabel!
    @IBOutlet weak var locationLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}

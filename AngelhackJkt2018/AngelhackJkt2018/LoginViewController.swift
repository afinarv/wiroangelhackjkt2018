//
//  ViewController.swift
//  AngelhackJkt2018
//
//  Created by Afina R. Vinci on 04/08/18.
//  Copyright © 2018 afinarv. All rights reserved.
//

import UIKit
import Firebase
import FirebaseAuth

class LoginViewController: UIViewController {
    
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
    }
    
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
        // Dispose of any resources that can be recreated.
    }
    
    
    
    
    @IBAction func doSignIn(_ sender: UIButton) {
        if let email = self.emailTextField.text, let password = self.passwordTextField.text {
            Auth.auth().signIn(withEmail: email, password: password) { (user, error) in
                // [START_EXCLUDE]
                
                if let error = error {
                    print("error = \(error)")
                    return
                }
                print("user = \(user)")
                self.performSegue(withIdentifier: "toHome", sender: self)
            }
        } else {
            print("email field cant be empty")
        }
    }
    
    
    
    
    
}


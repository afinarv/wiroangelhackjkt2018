//
//  IntroViewController.swift
//  AngelhackJkt2018
//
//  Created by Afina R. Vinci on 04/08/18.
//  Copyright © 2018 afinarv. All rights reserved.
//

import UIKit

class IntroViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func toLogin(_ sender: UIButton) {
        self.performSegue(withIdentifier: "toLogin", sender: self)
    }
    

}

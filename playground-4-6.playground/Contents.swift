//: Playground - noun: a place where people can play

import UIKit
import Foundation
/*
 var str = "Hello, playground"
 
 let char = "a"
 
 switch char {
 case "a":
 print("karakter pertama")
 case "b":
 print("karakter kedua")
 case "z":
 print("karakter terakhir")
 default:
 print("karakter tidak diketahui")
 }
 
 let group = "A"
 
 switch group {
 case "A":
 print("URU, RUS, KSA, EGY")
 case "B":
 print("SPA, POR, IRN, MOR")
 case "C":
 print("FRA, DEN, PER, AUS")
 case "D":
 print("CRO, ARG, NGA, ICE")
 default:
 print("grup lainnya")
 }
 
 let negara = "ARG"
 
 switch negara {
 case "URU", "RUS", "KSA", "EGY":
 print("A")
 case "SPA", "POR", "IRN", "MOR":
 print("B")
 case "FRA", "DEN", "PER", "AUS":
 print("C")
 case "CRO", "ARG", "NGA", "ICE":
 print("D")
 default:
 print("grup lainnya")
 }
 
 
 
 var aTupple = (1, 2, 3, "a", "b", "c")
 aTupple.3
 aTupple.2 = 6
 aTupple
 
 
 
 var (q, w, e) = (5, 6, 7)
 var t = (q, w, e)
 
 var musicGenre = Set<String>(["jazz", "blues"])
 var filmGenre = Set<String>()
 filmGenre.count
 filmGenre.insert("thriller")
 filmGenre.insert("comedy")
 filmGenre.capacity
 
 
 
 var anArray = ["gunadi", "anna", "milo"]
 for a in anArray {
 print(a)
 }
 
 
 
 var aDict: [String:Any] = ["name" : "Umi", "age" : 67]
 
 for i in aDict {
 //print(i)
 print("\(i.key) = \(i.value)")
 }
 print("\(aDict.keys)")
 
 
 let makhluk: [String:[String]]  = [
 "hewan": ["kambing", "kucing"],
 "unggas" : ["ayam", "bebek", "angsa"],
 "serangga" : ["nyamuk", "lalat", "lebah", "laron"]
 ]
 var total = 0
 for i in makhluk.keys {
 if let c = makhluk[i] {
 print("Jumlah \(i) ada \(c.count)")
 total += c.count
 }
 }
 print("Jumlah makhluk ada \(total)")
 
 */

func yaya(a: Int, b: String? ) -> String {
    //print("sas \(a) \(b ?? "")")
    return(String (describing: a))
}

let r = yaya(a: 9, b: nil)
print(r)


func vava(_ berapa: Int, _ kenapa: String) -> String {
    return("\(berapa), sasa \(kenapa)")
    //   return "io"
}

var tt = vava(7, "baik")


func penjumlahan(_ a: Int, _ b: Int) -> Int {
    return a + b
}

penjumlahan(2, 4)

func cekGenap(angka: Int) -> Bool {
    return angka % 2 == 0
}

if cekGenap(angka: 88789) {
    print("Nilai angka genap")
} else {
    print("Nilai angka tidak genap")
}


func cekTemperature(cel: Double) {
    switch cel {
    case 0.0..<29.0:
        print("suhu normal")
    case 29.0..<48.0:
        print("suhu hangat")
    case 48.0...100.0:
        print("suhu panas")
    default:
        print("Undefined")
    }
}

cekTemperature(cel: 29.0)

enum Satuan {
    case Celcius, Reamur, Kelvin, Fahrenheit
}

//MARK: function with enumeration
func convertTemp(temp: Double, from: Satuan, to: Satuan) -> Double {
    switch from {
    case .Celcius:
        switch to {
        case .Reamur:
            return temp/5*4
        case .Fahrenheit:
            return (temp*1.8)+32.0
        case .Kelvin:
            return temp + 273.15
        case .Celcius:
            return temp
        }
    case .Reamur:
        return convertTemp(temp: temp/4*5, from: .Celcius, to: to)
    case .Fahrenheit:
        return convertTemp(temp: (temp-32)/1.8, from: .Celcius, to: to)
    case .Kelvin:
        return convertTemp(temp: temp-273.15, from: .Celcius, to: to)
    }
}

print(convertTemp(temp: 273.15, from: .Kelvin, to: .Fahrenheit))



enum Planet {
    case merkurius
    case venus
    case bumi
    case mars
}

var planeti: Planet = .bumi

var yuyu = Planet.bumi

switch planeti {
case .bumi:
    break
case .venus:
    break
default:
    break
}

class person {
    var Name: String = ""
    var Age:Int = 0
    var Gender: String = ""
    init(Name: String, Age: Int, Gender:String)
    {
        self.Name=Name
        self.Gender=Gender
        self.Age=Age
    }
}
let p = person.init(Name: "Suresh Dasari", Age: 30, Gender: "Male")
print(p.Name,p.Age,p.Gender)



class Contoh {
    var count = 0
    
    static func tambah() {
        print("menambah")
    }
    
    func kurang() {
        print("mengurang")
    }
}

var t = Contoh()
t.kurang

Contoh.tambah()



//--PROPERTIES

struct GPS {
    var lat: Double
    var lon: Double
}

let aLocation = GPS(lat: 22.33, lon: 33.23)

aLocation.lat

let aTup = (22.22, 33.22)
aTup.0


//--NANOCHALLENGE: buat sebuah struct Buku dengan prop judul, penulis, halaman, harga

struct Buku {
    var judul: String
    var penulis: String
    var halaman: Int
    var harga: Int
    
    func cetak() {
        print("Buku favorit saya berjudul \(self.judul) dikarang oleh \(self.penulis). Buku ini sepanjang \(self.halaman) halaman dan bisa didapat dengan harga Rp\(self.harga),00.")
    }
}

let sebuahBuku = Buku(judul: "Sukses SBMPTN 2018", penulis: "Tim KKG Se-Indonesia", halaman: 909, harga: 86500)

print("Buku favorit saya berjudul \(sebuahBuku.judul) dikarang oleh \(sebuahBuku.penulis). Buku ini sepanjang \(sebuahBuku.halaman) halaman dan bisa didapat dengan harga Rp\(sebuahBuku.harga),00.")

sebuahBuku.cetak()


//--METHOD dan PROPERTY juga berlaku di dalam class

class PesawatTempur {
    var name: String = ""
    var hp: Int = 100
    var position: Int = 0
    
    func gerakKiri() {
        position -= 1
    }
    
    func gerakKanan() {
        position += 1
    }
    
    func tertembak() {
        hp -= 10
    }
    
    
}

let herkules = PesawatTempur()
herkules.gerakKiri()





//beda tupple dgn array? > fungsi2 array kyk count, insert,











//
//  ViewController.swift
//  Challenge Canteen Animation
//
//  Created by Afina R. Vinci on 13/07/18.
//  Copyright © 2018 afinarv. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    class Block: UIView {
        
    }
    
    var square = UIView()
    var screenWidth = Int()
    var screenHeight = Int()
    var allSquares = [[UIView]]()
    var initialCenter = CGPoint()
    
    @IBOutlet var panGesture: UIPanGestureRecognizer!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        screenWidth = Int(self.view.frame.width)
        screenHeight = Int(self.view.frame.height)
        self.view.addGestureRecognizer(panGesture)
        
        
        setupSquares()
        
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        print("touch begin")
        print(touches.first?.location(in: view))
        
    }
    
    override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?) {
        
    }
    
    func setupSquares() {
        
        let nSquareX = screenWidth/30
        let nSquareY = screenHeight/30
        
        var x = 0
        var y = 0
        for _ in 0...nSquareY {
            var xSquareArr = [UIView]()
            for i in 0...nSquareX {
                
                
                self.square = UIView(frame: CGRect(x: x, y: y, width: 30, height: 30))
                self.square.layer.borderWidth = 0.7
                self.square.layer.borderColor = UIColor.gray.cgColor
                self.square.backgroundColor = randomColor()
                self.square.tag = i
                print("tag = \(square.tag)")
                xSquareArr.append(square)
                //self.square.addGestureRecognizer(panGesture)
                self.view.addSubview(square)
                x += 30
            }
            allSquares.append(xSquareArr)
            xSquareArr.removeAll()
            x = 0
            y += 30
            
        }
        allSquares.forEach { (xSquares) in
            xSquares.forEach({ (square) in
                print(square.tag)
            })
            
        }
        
        
        
    }
    
    func randomColor() -> UIColor {
        let colors = [#colorLiteral(red: 0.5568627715, green: 0.3529411852, blue: 0.9686274529, alpha: 1), #colorLiteral(red: 0.9764705896, green: 0.850980401, blue: 0.5490196347, alpha: 1), #colorLiteral(red: 0.9098039269, green: 0.4784313738, blue: 0.6431372762, alpha: 1), #colorLiteral(red: 0.9568627477, green: 0.6588235497, blue: 0.5450980663, alpha: 1)]
        return colors[Int(arc4random_uniform(UInt32(colors.count)))]
    }
    
    
    @IBAction func panned(_ sender: UIPanGestureRecognizer) {
        
            
            var xIndex = Int(ceil((sender.location(in: view).x) / 30))-1
            var yIndex = Int(ceil((sender.location(in: view).y) / 30))-1
            
            var theSquare: UIView = allSquares[yIndex][xIndex]
            self.view.bringSubview(toFront: theSquare)
            theSquare.transform = CGAffineTransform(scaleX: 0.5, y: 0.5)
            theSquare.backgroundColor = randomColor()
      
            
        UIView.animate(withDuration: 0.4, delay: 0, options: .curveEaseInOut, animations: {
                theSquare.transform = CGAffineTransform(scaleX: 1.5, y: 1.5)
            }) { (true) in
                UIView.animate(withDuration: 0.4, animations: {
                theSquare.transform = CGAffineTransform.identity
                })
                
            }
        
        if let view = sender.view {
            UIView.animate(withDuration: 1) {
                view.backgroundColor = UIColor.black
            }
        }
    }
    
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
}


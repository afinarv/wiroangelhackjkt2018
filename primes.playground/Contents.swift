import Foundation

var nDivisible = 0
var upTo = 50

func dd(a: Int, b: String) {
    
    for b in 1...a {
        if a % b == 0 {
            nDivisible += 1
            if nDivisible > 2 {
                break
            }
        }
    }
    if nDivisible == 2 {
        print("\(a). \(b). prime. 🐳")
    } else {
        print("\(a). \(b). not prime. 🦁")
        
    }
}



for a in 1...upTo {
    if a % 2 != 0 {
        dd(a: a, b: "odd")
    } else {
        dd(a: a, b: "even")
    }
    nDivisible = 0
}

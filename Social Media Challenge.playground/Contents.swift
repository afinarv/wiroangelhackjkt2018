//: Playground - noun: a place where people can play

import UIKit

var str = "Hello, playground"


class User {
    var username: String
    var fullname: String
    var email: String
    init(username: String, fullname: String, email: String) {
        self.username = username
        self.fullname = fullname
        self.email = email
    }
}

class Post {
    var text: String
    var username: String
    var likers: [User] = []
    var comments: [Comment] = []
    init(text: String, username: String) {
        self.text = text
        self.username = username
    }
    
    func liked(by: User) {
        self.likers.append(by)
    }
    
    func add(newComment: Comment) {
        self.comments.append(newComment)
    }
    
    func getAllLikersString() -> [String] {
        var likersString: [String] = []
        for i in likers {
            likersString.append(i.username)
        }
        return likersString
    }
    
    func getAllCommenters() -> [String] {
        var commentersString: [String] = []
        for i in comments {
            commentersString.append(i.user.username)
        }
        return commentersString
    }
    
    func getPoints() -> Int {
        var points = 0
        let likePoints = likers.count * 5
        var commentPoints = 0
        let badWords = ["stupid", "moron", "goblok"]
        for i in comments {
            var badFlag = false
            let commentText = i.text
            
            for badWord in badWords {
                if commentText.range(of: badWord) != nil {
                    badFlag = true
                    break
                }
            }
            if badFlag {
                print("bad")
                commentPoints -= 3
            }
        }
        
        points = points + likePoints + commentPoints
        return points
    }
    
}

class Comment {
    var user: User
    var text: String
    init(user: User, text: String) {
        self.user = user
        self.text = text
    }
}

var firstUser = User(username: "budibermainbola", fullname: "Budi Subudi", email: "iniemailbudi@yahoo.com")

var secondUser = User(username: "anibelajarkoding", fullname: "Ani Supani", email: "inibukanemailbudi@yahoo.com")

var thirdUser = User(username: "jonijagokoding", fullname: "Joni Jonius", email: "inibukanemailbudi@yahoo.com")


var firstPost = Post(text: "Halo! First post enaknya apa ya?", username: "budibermainbola")

firstPost = Post(text: "Halohalo! Maaf yg tadi salah.", username: "budibermainbola")

var firstComment = Comment(user: secondUser, text: "Post foto selfie aja Bud.")

var secondComment = Comment(user: firstUser, text: "Oke deh Ni.")

var thirdComment = Comment(user: thirdUser, text: "moron lo Bud. stupid.")


firstPost.liked(by: secondUser)
firstPost.liked(by: thirdUser)
firstPost.liked(by: firstUser)
firstPost.add(newComment: firstComment)
firstPost.add(newComment: secondComment)
firstPost.add(newComment: thirdComment)

print(firstPost.getAllLikersString())
print(firstPost.getAllCommenters())
print(firstPost.getPoints())


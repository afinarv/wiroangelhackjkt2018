//
//  ViewController.swift
//  UIKit01-AppStructure
//
//  Created by Afina R. Vinci on 09/07/18.
//  Copyright © 2018 afinarv. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    // MARK: - Outlets
    @IBOutlet weak var titleBackgroundView: UIView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var segControl: UISegmentedControl!
    @IBOutlet weak var imageSwitch: UISwitch!
    @IBOutlet weak var button: UIButton!
    let fearText = "Fear"
    let courageText = "Courage"
    let fearImg = UIImage(named: "fear")
    let pebbleImg = UIImage(named: "pebble")
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        titleLabel.text = "Fear..."
        titleBackgroundView.backgroundColor = UIColor.cyan
        
    }
    
    @IBAction func buttonClicked(_ sender: Any) {
        print("button pressed here")
    }
    
    @IBAction func segControlClicked(_ sender: Any) {
        if segControl.selectedSegmentIndex == 0 {
            imageView.image = fearImg
        } else {
            imageView.image = pebbleImg
        }
    }
    
    @IBAction func switchChanged(_ sender: UISwitch) {
        if sender.isOn {
            titleLabel.text = fearText
        } else {
            titleLabel.text = courageText
        }
    }
    
    
    
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    



}


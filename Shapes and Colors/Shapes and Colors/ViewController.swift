//
//  ViewController.swift
//  Shapes and Colors
//
//  Created by Afina R. Vinci on 12/07/18.
//  Copyright © 2018 afinarv. All rights reserved.
//

import UIKit

class ViewController: UIViewController {


    @IBOutlet weak var redView: UIView!
    @IBOutlet weak var redShapeImg: UIImageView!
    var flashing = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(animate))
        
        view.addGestureRecognizer(tapGesture)
//        for i in 0...smallCircle.count-1 {
//            smallCircle[i].layer.cornerRadius = smallCircle[i].frame.size.width/2
//
//        }
//        for i in 0...medCircle.count-1 {
//            medCircle[i].layer.cornerRadius = medCircle[i].frame.size.width/2
//
//        }
        
        
        
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    @objc func animate() {
        

        UIView.animate(withDuration: 20, animations: {
            self.redShapeImg.transform = CGAffineTransform(scaleX: 75, y: 75)
        }) { (true) in
            self.btnFlash_Clicked()
            self.redView.isHidden = false
        }
        print("kkk")
    }
    
    func btnFlash_Clicked() {
        flashing = true
        if !flashing{
            self.redView.alpha = 1.0
            UIView.animate(withDuration: 0.5, delay: 0.0, options: [.curveEaseInOut, .repeat, .autoreverse, .allowUserInteraction], animations: {() -> Void in
                self.redView.alpha = 0.0
            }, completion: {(finished: Bool) -> Void in
            })
            flashing = true
        }
        else{
            UIView.animate(withDuration: 0.1, delay: 0.0, options: [.curveEaseInOut, .beginFromCurrentState], animations: {() -> Void in
                self.redView.alpha = 1.0
            }, completion: {(finished: Bool) -> Void in
            })
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
        // Dispose of any resources that can be recreated.
    }


}


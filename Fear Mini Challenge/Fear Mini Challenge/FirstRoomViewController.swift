//
//  FirstRoomViewController.swift
//  Fear Mini Challenge
//
//  Created by Afina R. Vinci on 18/07/18.
//  Copyright © 2018 afinarv. All rights reserved.
//

import UIKit

class FirstRoomViewController: UIViewController {
    @IBOutlet weak var coverView: UIView!
    @IBOutlet weak var backArrowImgView: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        UIView.animate(withDuration: 2.0, delay: 2, options: .curveLinear, animations: {
            self.coverView.alpha = 0.0
        }, completion: nil)
        shakeArrow()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
       // audioPlayer.setVolume(0.5, fadeDuration: 1.5)
    }
    
    //MARK: - shake arrow
    func shakeArrow() {
        UIView.animate(withDuration: 0.8, delay: 0, options: [.repeat,.autoreverse], animations:{
            self.backArrowImgView.frame.origin.x += 20
            self.backArrowImgView.frame.origin.x -= 20
        }, completion: nil)
    }
    
    
    
    
    
    

 
    
}

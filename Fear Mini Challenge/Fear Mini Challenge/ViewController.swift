//
//  ViewController.swift
//  Fear Mini Challenge
//
//  Created by Afina R. Vinci on 16/07/18.
//  Copyright © 2018 afinarv. All rights reserved.
//

import UIKit
import AVFoundation

class ViewController: UIViewController, UITableViewDelegate {
    @IBOutlet weak var lorongImgView: UIImageView!
    var soundPlayer: AVAudioPlayer!
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var coverView: UIView!
    //var isFirstOpen = true
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        coverView.alpha = 1
        coverView.isHidden = false
            UIView.animate(withDuration: 2, delay: 0, options: .curveLinear, animations: {
                self.coverView.alpha = 0
        })
    }
    
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        //showScreenshotEffect()
        let impact = UIImpactFeedbackGenerator()
        impact.impactOccurred()
        playSound()
        fadeContainer()
        
    }
    
    //MARK: - blink effect
    
    func showScreenshotEffect() {
        
        let snapshotView = UIView()
        snapshotView.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(snapshotView)
        
        let constraints:[NSLayoutConstraint] = [
            snapshotView.topAnchor.constraint(equalTo: view.topAnchor),
            snapshotView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            snapshotView.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            snapshotView.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor)
        ]
        NSLayoutConstraint.activate(constraints)
        
        snapshotView.backgroundColor = UIColor.black
        snapshotView.alpha = 0.0
        
        UIView.animate(withDuration: 2.2, animations: {
            snapshotView.alpha = 0.8
        }) { _ in
            UIView.animate(withDuration: 2.2, animations: {
                snapshotView.alpha = 0.0
            }, completion: { (true) in
                self.showScreenshotEffect()
            })
            //snapshotView.removeFromSuperview()
        }
    }
    
    func fadeContainer() {
        containerView.translatesAutoresizingMaskIntoConstraints = false
        containerView.alpha = 0.0
        UIView.animate(withDuration: 2.2, animations: {
            self.containerView.alpha = 0.8
        }) { _ in
            UIView.animate(withDuration: 2.2, animations: {
                self.containerView.alpha = 0.0
            }, completion: { (true) in
                self.fadeContainer()
            })
            //snapsh
        }
    }
    
    //MARK: - play sound
    
    func playSound() {
        
        let path = Bundle.main.path(forResource: "exampleSound", ofType:"mp3")!
        let url = URL(fileURLWithPath: path)
        
        do {
            let sound = try AVAudioPlayer(contentsOf: url)
            soundPlayer = sound
            sound.setVolume(0.0, fadeDuration: 2.0)
            sound.play()
            
        } catch {
            // couldn't load file :(
            print("tidak bisa keluar suara")
        }
        
        
        var array: Array<UIView> = []
        /*
        for index in 0..<2 {
            array.append(isian(UInt(index)))
        }*/
    }
    
    
    //MARK: fading segue
    
    @IBAction func performSegue(_ sender: UIButton) {
        performSegue(withIdentifier: "toDestination", sender: self)
    }
    
    @IBAction func prepareForUnwind(segue: UIStoryboardSegue) {
        
    }
    


}


//
//  ChallengeViewController.swift
//  Basic Animation
//
//  Created by Afina R. Vinci on 11/07/18.
//  Copyright © 2018 afinarv. All rights reserved.
//

import UIKit

class ChallengeViewController: UIViewController {

    @IBOutlet weak var squareView: UIView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(changeColor))
        let swipeGesture = UISwipeGestureRecognizer(target: self, action: #selector(changeXY))
        swipeGesture.direction = .down
        squareView.addGestureRecognizer(tapGesture)
        squareView.addGestureRecognizer(swipeGesture)
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    @objc func changeColor() {
        print("tap")
        let randomR = CGFloat(arc4random_uniform(UInt32(255)))
        let randomG = CGFloat(arc4random_uniform(UInt32(255)))
        let randomB = CGFloat(arc4random_uniform(UInt32(255)))
        let randomColor = UIColor(red: randomR/255, green: randomG/255, blue: randomB/255, alpha: 1.0)
        UIView.animate(withDuration: 1) {
            self.squareView.backgroundColor = randomColor
        }
    }
    
    
    @objc func changeXY() {
        print("swipe")
        let screenWidth = self.view.frame.width
        let screenHeight = self.view.frame.height
        let squareWidth = self.squareView.frame.width
        let squareHeight = self.squareView.frame.height
        print(screenWidth)
        print(screenHeight)
        
        let randomX = Int(arc4random_uniform(UInt32(screenWidth-squareWidth)))
        let randomY = Int(arc4random_uniform(UInt32(screenHeight-squareHeight)))
        UIView.animate(withDuration: 3) {
            self.squareView.frame = CGRect(x: randomX, y: randomY, width: Int(squareWidth), height: Int(squareHeight))
        }
        
        
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

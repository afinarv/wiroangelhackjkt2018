//
//  AdventureViewController.swift
//  Basic Animation
//
//  Created by Afina R. Vinci on 11/07/18.
//  Copyright © 2018 afinarv. All rights reserved.
//

import UIKit

class AdventureViewController: UIViewController {

    
    @IBOutlet weak var redView: UIView!
    var obstacle = UIView()
    var isRunning = false
    
    override func viewDidLoad() {
        super.viewDidLoad()

        let startGesture = UITapGestureRecognizer(target: self, action: #selector(startGame))
        redView.addGestureRecognizer(startGesture)
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @objc func startGame() {
        isRunning = !isRunning
        if isRunning {
            spawnObstacle()
        }
        
    }
    
    func spawnObstacle() {
        let screenWidth = self.view.frame.width
        let screenHeight = self.view.frame.height
        let randomX = CGFloat(arc4random_uniform(UInt32(screenWidth-40)))
        obstacle = UIView(frame: CGRect(x: randomX, y: 0, width: 40.0, height: 250.0))
        obstacle.backgroundColor = UIColor.black
        self.view.addSubview(obstacle)
        UIView.animate(withDuration: 1, delay: 0, options: .curveLinear, animations: {
            self.obstacle.frame = CGRect(x: randomX, y: screenHeight, width: self.obstacle.frame.width, height: self.obstacle.frame.height)
          
                self.detectCollision()
            
        }) { (true) in
            self.spawnObstacle()
        }
    }
    
  
    
    func detectCollision() {
        print(obstacle.frame)
        print(redView.frame)
        if redView.frame.contains(obstacle.frame) {
            print("tabrakan")
            
        }
    }
    

}

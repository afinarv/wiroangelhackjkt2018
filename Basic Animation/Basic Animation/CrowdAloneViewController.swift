//
//  CrowdAloneViewController.swift
//  Basic Animation
//
//  Created by Afina R. Vinci on 11/07/18.
//  Copyright © 2018 afinarv. All rights reserved.
//

import UIKit

class CrowdAloneViewController: UIViewController {

    @IBOutlet weak var crowdButton: UIButton!
    @IBOutlet weak var aloneButton: UIButton!
    var bubbles = [UIView]()
    enum State {
        case crowd
        case alone
    }
    var state: State = .crowd
    var delay: Double = 0
    
    var screenWidth = Int()
    var screenHeight = Int()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        screenWidth = Int(self.view.frame.width)
        screenHeight = Int(self.view.frame.height)
        setupGestureRecognizers()
        setupBubbles(withSize: 20)
        // Do any additional setup after loading the view.
    }

    func setupGestureRecognizers() {
        let startTap = UITapGestureRecognizer(target: self, action: #selector(starting))
        self.view.addGestureRecognizer(startTap)
    }
    
    func setupBubbles(withSize: Int) {
        self.bubbles.removeAll()
        
        let bubbleColor = setupRandomColor()
        var gap = 0
        let randomGap = randomX()
        let n = arc4random_uniform(3)
        for i in 0...n {
            print(i)
            gap += randomGap
            let oneBubble = UIView(frame: setupRandomRect(withPadding: withSize, withX: gap))
            oneBubble.backgroundColor = bubbleColor
            oneBubble.layer.cornerRadius = CGFloat(withSize/2)
            bubbles.append(oneBubble)
            self.view.addSubview(oneBubble)
            
            UIView.animate(withDuration: 3, delay: TimeInterval(delay), options: .curveEaseInOut, animations: {
                print(self.bubbles.count)
                let x = oneBubble.frame.minX
                let size = oneBubble.frame.size.width
                oneBubble.frame = CGRect(x: x, y: -20, width: size, height: size)
                
            }, completion: nil)
        }
    }
    
    func randomX() -> Int {
        let randomX = Int(arc4random_uniform(UInt32(100) + 30))
        return randomX
    }
    
    func setupRandomRect(withPadding: Int, withX: Int) -> CGRect{
        //let randomX = Int(arc4random_uniform(UInt32(screenWidth-withPadding)))
        let randomRect = CGRect(x: withX, y: screenHeight, width: withPadding, height: withPadding)
        return randomRect
    }
    
    func setupRandomColor() -> UIColor {
        let colors = [#colorLiteral(red: 0.9098039269, green: 0.4784313738, blue: 0.6431372762, alpha: 1), #colorLiteral(red: 0.3411764801, green: 0.6235294342, blue: 0.1686274558, alpha: 1), #colorLiteral(red: 0.2392156869, green: 0.6745098233, blue: 0.9686274529, alpha: 1), #colorLiteral(red: 0.9607843161, green: 0.7058823705, blue: 0.200000003, alpha: 1)]
        let randomR = CGFloat(arc4random_uniform(UInt32(255)))
        let randomG = CGFloat(arc4random_uniform(UInt32(255)))
        let randomB = CGFloat(arc4random_uniform(UInt32(255)))
        let randomColor = UIColor(red: randomR/255, green: randomG/255, blue: randomB/255, alpha: 1.0)
        return colors[Int(arc4random_uniform(UInt32(colors.count)))]
    }
    
    func setupButtonColor() {
        
        switch state {
        case .alone:
            crowdButton.setTitleColor(#colorLiteral(red: 0.6000000238, green: 0.6000000238, blue: 0.6000000238, alpha: 1), for: .normal)
            aloneButton.setTitleColor(#colorLiteral(red: 0, green: 0, blue: 0, alpha: 1), for: .normal)
        case .crowd:
            crowdButton.setTitleColor(#colorLiteral(red: 0, green: 0, blue: 0, alpha: 1), for: .normal)
            aloneButton.setTitleColor(#colorLiteral(red: 0.6000000238, green: 0.6000000238, blue: 0.6000000238, alpha: 1), for: .normal)
        }
    }
    
    @IBAction func buttonPressed(sender: UIButton) {
        if sender.tag == 0 {
            state = .crowd
            setupButtonColor()
        } else {
            state = .alone
            setupButtonColor()
        }
    }
    
    @objc func starting() {
        print("starting")
    }

    func setupBubblesAnimation() {
        UIView.animate(withDuration: 1, delay: 0, options: .curveEaseInOut, animations: {
            
            for i in 0...self.bubbles.count {
                print(self.bubbles.count)
                let x = self.bubbles[i].frame.minX
                let size = self.bubbles[i].frame.size.width
                self.bubbles[i].frame = CGRect(x: 20, y: 0, width: size, height: size)
            }
        }, completion: nil)
    }
    
    
    
}

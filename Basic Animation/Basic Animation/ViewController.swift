//
//  ViewController.swift
//  Basic Animation
//
//  Created by Afina R. Vinci on 11/07/18.
//  Copyright © 2018 afinarv. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var blueView: UIView!
    @IBOutlet weak var aButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(startAnimate))
        blueView.addGestureRecognizer(tapGesture)
        
        
        
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    @objc func startAnimate() {
        UIView.animate(withDuration: 3) {
            
            //self.blueView.frame = CGRect(x: 0, y: 0, width: 90, height: 90)
            self.blueView.center = self.view.center
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}


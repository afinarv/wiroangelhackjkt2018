//
//  TransformersViewController.swift
//  Basic Animation
//
//  Created by Afina R. Vinci on 11/07/18.
//  Copyright © 2018 afinarv. All rights reserved.
//

import UIKit

class TransformersViewController: UIViewController {

    @IBOutlet weak var squareView: UIView!
    enum State {
        case big
        case small
    }
    var squareState: State = .small
    
    override func viewDidLoad() {
        super.viewDidLoad()

        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(transform))
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @objc func transform() {
        switch squareState {
        case .small:
            UIView.animate(withDuration: 1) {
                self.squareView.transform = CGAffineTransform.init(scaleX: 2, y: 2)
            }
            self.squareState = .big
        case .big:
            UIView.animate(withDuration: 1) {
                self.squareView.transform = CGAffineTransform.init(scaleX: 2, y: 2)
            }
            self.squareState = .small
        }
    }
    
    
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
